﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class BoidSystem : MonoBehaviour
{
    [Inject]
    Boid.Pool boidPool;

    [SerializeField]
    private Transform sceneParent;

    [SerializeField]
    public int boidNumber;

    [SerializeField]
    public float speed = 0.01f;

    [SerializeField]
    public float angleWobbling = 0.05f;

    [SerializeField]
    public float separationDistance = 5f;

    [SerializeField]
    public float cohesionDistance = 10f;

    [SerializeField]
    [Range(0f, 360f)]
    public float awarenessAngle = 30f;

    [SerializeField]
    public float maxDistance = 100f;

    [SerializeField]
    [Range(0f, 1f)]
    public float separationFactor = 1f;

    [SerializeField]
    [Range(0f, 1f)]
    public float cohesionFactor = 1f;

    [SerializeField]
    [Range(0f, 1f)]
    public float alignmentFactor = 1f;

    [SerializeField]
    [Range(0f, 1f)]
    public float goBackFactor = 1f;

    void Start()
    {
        for (int i = 0; i < boidNumber; ++i)
        {
            boidPool.Spawn(
                sceneParent,
                new Vector3(Random.Range(-5.0f, 5.0f), Random.Range(0.0f, 5.0f), Random.Range(-5.0f, 5.0f)),
                Quaternion.Euler(Random.Range(0.0f, 360.0f), Random.Range(0.0f, 360.0f), Random.Range(0.0f, 360.0f)));
        }
    }
}
