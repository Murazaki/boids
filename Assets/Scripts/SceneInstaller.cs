using UnityEngine;
using Zenject;

public class SceneInstaller : MonoInstaller
{
    [SerializeField]
    private GameObject boidPrefab;

    [SerializeField]
    private BoidSystem boidSystem;

    public override void InstallBindings()
    {
        Container.Bind<BoidSystem>().FromInstance(boidSystem).AsSingle();
        Container.BindMemoryPool<Boid, Boid.Pool>()
             .WithInitialSize(boidSystem.boidNumber)
             .FromComponentInNewPrefab(boidPrefab)
             .UnderTransformGroup("Boids");
    }
}