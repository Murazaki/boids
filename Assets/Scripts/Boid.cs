﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class Boid : MonoBehaviour
{
    public class Pool : MonoMemoryPool<Transform, Vector3, Quaternion, Boid>
    {
        public List<Boid> Boids { get; private set; } = new List<Boid>();
        protected override void Reinitialize(Transform parent, Vector3 position, Quaternion rotation, Boid boid)
        {
            boid.Reset(parent, position, rotation);
        }

        protected override void OnSpawned(Boid boid)
        {
            base.OnSpawned(boid);

            Boids.Add(boid);
        }

        protected override void OnDespawned(Boid boid)
        {
            Boids.Remove(boid);

            base.OnDespawned(boid);
        }
    }


    [Inject]
    private Boid.Pool boidPool;

    [Inject]
    private BoidSystem boidSystem;

    public void Reset(Transform parent, Vector3 position, Quaternion rotation)
    {
        transform.SetParent(parent);
        transform.localPosition = position;
        transform.localRotation = rotation;
    }

    void Update()
    {
        float distance = 0;
        float angle = 0;
        Vector3 relativeBoidPosition = Vector3.zero;

        Vector3 crowdCohesion = Vector3.zero;
        Vector3 crowdSeparation = Vector3.zero;
        Vector3 crowdAlignment = Vector3.zero;
        Vector3 goBack = Vector3.zero;

        Vector3 zoneNormal = transform.parent.position - transform.position;
        float zoneDistance = Vector3.Distance(transform.parent.position, transform.position);
        float zoneAngle = Mathf.Abs(Vector3.Angle(zoneNormal, transform.forward));
        
        if ((zoneAngle > 90f) & (zoneDistance > boidSystem.maxDistance))
        {
            goBack += transform.forward - 2f * Vector3.Dot(transform.forward, zoneNormal) * zoneNormal;
        }

        foreach (Boid boid in boidPool.Boids)
        {
            relativeBoidPosition = boid.transform.position - transform.position;
            distance = Vector3.Distance(boid.transform.position, transform.position);
            angle = Mathf.Abs(Vector3.Angle(relativeBoidPosition, transform.forward));

            if (boid != this &&
                distance < boidSystem.cohesionDistance &&
                angle < boidSystem.awarenessAngle / 2)
            {
                if (distance < boidSystem.separationDistance)
                {
                    crowdSeparation += (boidSystem.separationDistance / distance) * (- relativeBoidPosition + 2f * Vector3.Dot(relativeBoidPosition, transform.forward) * transform.forward);
                    crowdAlignment += boid.transform.forward;
                }
                else
                {
                    crowdCohesion += relativeBoidPosition;
                }
            }
        }

        crowdCohesion /= boidPool.Boids.Count;
        crowdSeparation /= boidPool.Boids.Count;
        crowdAlignment /= boidPool.Boids.Count;

        transform.Translate(boidSystem.speed * Vector3.forward);

        Vector3 newDirection = crowdCohesion * boidSystem.cohesionFactor +
                               crowdSeparation * boidSystem.separationFactor +
                               crowdAlignment * boidSystem.alignmentFactor +
                               goBack * boidSystem.goBackFactor +
            transform.forward +
            new Vector3(Random.Range(-boidSystem.angleWobbling / 2, boidSystem.angleWobbling / 2),
                        Random.Range(-boidSystem.angleWobbling / 2, boidSystem.angleWobbling / 2),
                        Random.Range(-boidSystem.angleWobbling / 2, boidSystem.angleWobbling / 2));

        Debug.Log($"crowdCohesion : {crowdCohesion} crowdSeparation : {crowdSeparation} crowdAlignment : {crowdAlignment} newDirectionDelta : {newDirection - transform.forward}");

        Debug.DrawRay(transform.position, crowdCohesion, Color.cyan);
        Debug.DrawRay(transform.position, crowdSeparation, Color.green);
        Debug.DrawRay(transform.position, crowdAlignment, Color.yellow);
        Debug.DrawRay(transform.position, newDirection, Color.red);
        Debug.DrawRay(transform.position, transform.forward, Color.blue);

        transform.rotation = Quaternion.LookRotation(newDirection);
    }
}
